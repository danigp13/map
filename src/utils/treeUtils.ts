import { RenderTree } from "../components/Tree";

/**
 * 
 * @param node Given a tree and a filter condition, get the nodes that fullfilled the condition
 * @param filterCondition 
 * @returns 
 */
export const getNodes = (node: RenderTree[], filterCondition: (node: {id: string, type: string}) => boolean): {id: string, type: string}[] => {
  return node?.flatMap(x => [...[{id: x.id, type: x?.children ? "parent" : "leaf"}], ...x?.children ? getNodes(x?.children as RenderTree[], filterCondition) : []]).filter(filterCondition);
}


/**
 * Given a node, returns if is a parent node
 * @param node 
 * @returns 
 */
export const isParent = (node: {id: string, type: string}): boolean => {
  return node.type === "parent";
}

/**
 * 
 * @param node Given a node, returns if is a leaf node
 * @returns 
 */
export const isLeaf = (node: {id: string, type: string}): boolean => {
  return node.type === "leaf";
}


// Input data for Tree component. It haven´t added to json server because is not a json format itself
export const dataTree: RenderTree = {
  name : 'Management Solution',
  id : 'Management Solution',
  children: [
    {
      name : 'UK',
      id : 'UK',
      children: [
        {
          name : 'Customer Group1',
          id : 'Customer Group 1',
          children: [
            {
              name : 'Customer 1',
              id : 'Customer 1',
              children: [
                {
                  name : 'London Shopping Mail',
                  id : 'London Shopping Mail',
                  children: [
                    {
                      name : 'Cars Inc.',
                      id : 'Car'
                    }
                  ]
                }
              ]
            }
          ]
        }
      ]
    },
    {
      name : 'DE',
      id : 'DE',
      children: [
        {
          name : 'Customer Group 3',
          id : 'Customer Group 3',
          children: [
            {
              name : 'Customer 3 Retail',
              id : 'Customer 3 Retail',
              children: [
                {
                  name : 'City Hospital',
                  id : 'Ambulance'
                }
              ]
            }
          ]
        }
      ]
    },
    {
      name : 'GB',
      id : 'GB',
      children: [
        {
          name : 'Customer Group 4',
          id : 'Customer Group 4',
          children: [
            {
              name : 'Trucks',
              id : 'Truck'
            }
          ]
        }
      ]
    }
  ]
};