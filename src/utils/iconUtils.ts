import iconCar from "../svg/car.svg";
import iconAmbulance from "../svg/ambulance.svg";
import iconTruck from "../svg/truck.svg";
import icon from 'leaflet/dist/images/marker-icon.png';
import L from "leaflet";

export const DefaultIcon = L.icon({
  iconUrl: icon,
});

export const CarIcon = L.icon({
  iconUrl: iconCar,
  iconSize: [35, 46], // size of the icon
  iconAnchor: [17, 46], // point of the icon which will correspond to marker's location
  popupAnchor: [-3, -40] // point from which the popup should open relative to the iconAnchor
});

export const AmbulanceIcon = L.icon({
  iconUrl: iconAmbulance,
  iconSize: [35, 46], // size of the icon
  iconAnchor: [17, 46], // point of the icon which will correspond to marker's location
  popupAnchor: [-3, -40] // point from which the popup should open relative to the iconAnchor
});

export const TruckIcon = L.icon({
  iconUrl: iconTruck,
  iconSize: [35, 46], // size of the icon
  iconAnchor: [17, 46], // point of the icon which will correspond to marker's location
  popupAnchor: [-3, -40] // point from which the popup should open relative to the iconAnchor
});