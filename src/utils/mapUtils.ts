import { LatLngBounds } from "leaflet";
import { LngLat, LngLatBounds } from "../model";

/**
 * 
 * @param elements Function that get the bounds of the map of given points
 * @returns 
 */
export const getBoundsFromPoints = (elements: LngLat[]): LngLatBounds => {

  // I have set these coordinates by default for this example
  const defaultBounds: LngLatBounds = {
    ne: {
      lat: 51.524552215462975,
      lng: 0.03313064575195313
    },
    sw: {
      lat: 51.485445119088524,
      lng: -0.21320343017578128
    }
  }
  if (!elements || !elements.length) {
    return defaultBounds;
  }
  const elemsWithValidCoords = elements.filter(elem => elem.lng != null && elem.lat != null);

  if (!elemsWithValidCoords || !elemsWithValidCoords.length) {
    return defaultBounds;
  }
  const viewPortBounds: LngLatBounds = {
    ne: {
      lng: elemsWithValidCoords[0].lng,
      lat: elemsWithValidCoords[0].lat,
    },
    sw: {
      lng: elemsWithValidCoords[0].lng,
      lat: elemsWithValidCoords[0].lat,
    },
  };
  elements.forEach(elem => {
    if (elem.lat == null || elem.lng == null) {
      return;
    }
    if (elem.lat > viewPortBounds.ne.lat) {
      viewPortBounds.ne.lat = elem.lat;
    } else if (elem.lat < viewPortBounds.sw.lat) {
      viewPortBounds.sw.lat = elem.lat;
    }
    if (elem.lng > viewPortBounds.ne.lng) {
      viewPortBounds.ne.lng = elem.lng;
    } else if (elem.lng < viewPortBounds.sw.lng) {
      viewPortBounds.sw.lng = elem.lng;
    }
  });
  return viewPortBounds;
};

/**
 * 
 * @param bounds Map the bounds to LatLngBounds that leaflet map accepts
 * @returns 
 */
export const mapBounds = (bounds: LngLatBounds): LatLngBounds => {
  return new LatLngBounds(bounds.sw, bounds.ne);
}

/**
 * Map array of coordinates to LngLat
 * @param latlng
 * @returns 
 */
export const mapLocation = (latlng: number[]): LngLat => {
  return {
    lat: latlng[0],
    lng: latlng[1],
  };
};