import axios, { AxiosError, AxiosResponse } from "axios";
import { DeviceOption } from "../../model";
import { buildErrorMessage, errorCodes, serverURL } from "../apiUtils";
import { mapDeviceOptionFromDTO, DeviceDTO } from "../apimodel";

export const searchDevices = (types: string[]) => (inputString: string): Promise<DeviceOption[]> => {
  const searchDevicesURL = `${serverURL}/devices/?q=${inputString}${types?.length ? `&type=${types.join('&type=')}` : ``}`;
  return axios.get(searchDevicesURL)
    .then(resolveDevices)
    .catch(handleErrors);
};

const resolveDevices = (response: AxiosResponse<DeviceDTO[]>): DeviceOption[] => {
  return response?.data && Array.isArray(response?.data)
    ? (response.data as DeviceDTO[])
      .map<DeviceOption>(mapDeviceOptionFromDTO)
    : [];
};

const handleErrors = (error: AxiosError) => {
  throw new Error(buildErrorMessage(errorCodes.ERROR_SEARCH_DEVICES)(error));
};