import axios from 'axios';
import { DeviceOption } from '../../model';
import { DeviceDTO, DeviceTypeDTO } from '../apimodel';
import { searchDevices } from './searchDevices';


describe('searchDevices', () => {

  afterEach(() => {
    jest.restoreAllMocks();
  });

  it('should fetch successfully data from an API', async () => {
    const getMock = jest.spyOn(axios, 'get');
    const mockedData: DeviceDTO[] = [
      {
        imei:"imei1",
        name:"car1",
        latitude: 51.505,
        longitude: -0.08,
        accelerometer: 2,
        temperature:38.2,
        battery: 80,
        historical:[
          [51.505, -0.091], [51.51, -0.11], [51.51, -0.121]
        ],
        type: DeviceTypeDTO.Car
      },
      {
        imei:"imei2",
        name:"car2",
        latitude: 51.505,
        longitude: -0.1,
        accelerometer: 2,
        temperature:38.2,
        battery: 80,
        historical:[
          [51.505, -0.092], [51.51, -0.12], [51.51, -0.122]
        ],
        type: DeviceTypeDTO.Car
      },
    ];
    const expectedData: DeviceOption[] = [
      {
        label: "car1",
        value: "imei1",
      },
      {
        label: "car2",
        value: "imei2",
      },
    ];

    const mockedResponse = {
      data: mockedData,
    };

    getMock.mockImplementation(() => Promise.resolve(mockedResponse));

    const response = await searchDevices([])('car');

    expect(response.length).toEqual(2);
    expect(response).toEqual(expectedData);
  });

  it('should fetch erroneous data from an API', async () => {

    const getMock = jest.spyOn(axios, 'get');
    const errorMessage = 'ERROR. Could not search devices. Unexpected error, request was not sent';

    getMock.mockImplementationOnce(() =>
      Promise.reject(new Error(errorMessage)),
    );

    await expect(searchDevices([])('car')).rejects.toThrow(errorMessage);
  });
});