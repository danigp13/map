import axios from 'axios';
import { DeviceType, Device } from '../../model';
import { DeviceDTO, DeviceTypeDTO } from '../apimodel';
import { fetchDevices } from './fetchDevices';


describe('fetchDevices', () => {

  afterEach(() => {
    jest.restoreAllMocks();
  });

  it('should fetch successfully data from an API', async () => {
    const getMock = jest.spyOn(axios, 'get');
    const mockedData: DeviceDTO[] = [
      {
        imei:"imei1",
        name:"car1",
        latitude: 51.505,
        longitude: -0.08,
        accelerometer: 2,
        temperature:38.2,
        battery: 80,
        historical:[
          [51.505, -0.091], [51.51, -0.11], [51.51, -0.121]
        ],
        type: DeviceTypeDTO.Car
      },
      {
        imei:"imei2",
        name:"car2",
        latitude: 51.505,
        longitude: -0.1,
        accelerometer: 2,
        temperature:38.2,
        battery: 80,
        historical:[
          [51.505, -0.092], [51.51, -0.12], [51.51, -0.122]
        ],
        type: DeviceTypeDTO.Car
      },
    ];
    const expectedData: Device[] = [
      {
        imei:"imei1",
        name:"car1",
        location:[-0.08, 51.505],
        accelerometer: 2,
        temperature:38.2,
        battery: 80,
        historical:[
          [51.505, -0.091], [51.51, -0.11], [51.51, -0.121]
        ],
        type: DeviceType.Car
      },
      {
        imei:"imei2",
        name:"car2",
        location:[-0.1, 51.505],
        accelerometer: 2,
        temperature:38.2,
        battery: 80,
        historical:[
          [51.505, -0.092], [51.51, -0.12], [51.51, -0.122]
        ],
        type: DeviceType.Car
      },
    ];

    const mockedResponse = {
      data: mockedData,
    };

    getMock.mockImplementation(() => Promise.resolve(mockedResponse));

    const response = await fetchDevices();

    expect(response.length).toEqual(2);
    expect(response).toEqual(expectedData);
  });

  it('should fetch erroneous data from an API', async () => {

    const getMock = jest.spyOn(axios, 'get');
    const errorMessage = 'ERROR. Could not fetch devices. Unexpected error, request was not sent';

    getMock.mockImplementationOnce(() =>
      Promise.reject(new Error(errorMessage)),
    );

    await expect(fetchDevices()).rejects.toThrow(errorMessage);
  });
});