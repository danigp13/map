import axios, { AxiosError, AxiosResponse } from "axios";
import { Device } from "../../model";
import { buildErrorMessage, errorCodes, serverURL } from "../apiUtils";
import { mapDeviceFromDTO, DeviceDTO } from "../apimodel";

export const fetchDevices = (types?: string[]): Promise<Device[]> => {
  const fetchDevicesURL = `${serverURL}/devices${types?.length ? `?type=${types.join('&type=')}` : ``}`;
  return axios.get(fetchDevicesURL)
    .then(resolveDevices)
    .catch(handleErrors);
};

const resolveDevices = (response: AxiosResponse<DeviceDTO[]>): Device[] => {
  return response?.data && Array.isArray(response?.data)
    ? (response.data as DeviceDTO[])
      .map<Device>(mapDeviceFromDTO)
    : [];
};

const handleErrors = (error: AxiosError) => {
  throw new Error(buildErrorMessage(errorCodes.ERROR_FETCH_DEVICES)(error));
};

export const fetchDevicesAwait = async(types?: string[]): Promise<Device[]> => {
  const fetchDevicesURL = `${serverURL}/devices${types?.length ? `?type=${types.join('&type=')}` : ``}`;
  const result = await axios.get(fetchDevicesURL);
  return resolveDevices(result);
  //return axios.get(fetchDevicesURL)
  //  .then(resolveDevices)
  //  .catch(handleErrors);
};