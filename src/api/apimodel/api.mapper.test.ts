import { Device, DeviceOption, DeviceType } from "../../model";
import { mapDeviceFromDTO, mapDeviceOptionFromDTO, mapDeviceTypeFromDTO } from "./api.mapper";
import { DeviceDTO, DeviceTypeDTO } from "./apiModel";

describe('mapDeviceFromDTO', () => {

  const deviceDTO: DeviceDTO = {
    imei:"imei1",
    name:"car1",
    latitude: 51.505,
    longitude: -0.08,
    accelerometer: 2,
    temperature:38.2,
    battery: 80,
    historical:[
      [51.505, -0.091], [51.51, -0.11], [51.51, -0.121]
    ],
    type: DeviceTypeDTO.Car
  };

  it('should map devices from the DTO model into Device defined main model', () => {
    // Arrange
    const expectedResult: Device = {
      imei:"imei1",
      name:"car1",
      location:[-0.08, 51.505],
      accelerometer: 2,
      temperature:38.2,
      battery: 80,
      historical:[
        [51.505, -0.091], [51.51, -0.11], [51.51, -0.121]
      ],
      type: DeviceType.Car
    };
    // Act
    const result = mapDeviceFromDTO(deviceDTO);
    // Assert
    expect(result).toEqual(expectedResult);
  });

  it('should map devices from the DTO model into the Device Option', () => {
    // Arrange
    const expectedResult: DeviceOption = {
      value:"imei1",
      label:"car1",
    };
    // Act
    const result = mapDeviceOptionFromDTO(deviceDTO);
    // Assert
    expect(result).toEqual(expectedResult);
  });

  it('should map DeviceType from the DTO model into DeviceType defined in main model', () => {
    const devicesTypesDTO = [ DeviceTypeDTO.Car, DeviceTypeDTO.Ambulance, DeviceTypeDTO.Truck ];
    const expectedDevicesTypes = [ DeviceType.Car, DeviceType.Ambulance, DeviceType.Truck ];
    const result = devicesTypesDTO.map(mapDeviceTypeFromDTO);
    expect(result).toEqual(expectedDevicesTypes);
  });

  it('should throw a custom error if the DeviceType from DTO received cannot be mapped', (done) => {
    const devicesTypesDTO = 'unknown_device_type';
    try {
      mapDeviceTypeFromDTO(devicesTypesDTO as DeviceTypeDTO);
    } catch (e: any) {
      // eslint-disable-next-line jest/no-conditional-expect
      expect(e.message).toEqual("Unknown device type received from backend service");
      done();
    }
  });

});