import { Device, DeviceOption, DeviceType } from "../../model";
import { DeviceDTO, DeviceTypeDTO } from "./apiModel";


export const mapDeviceTypeFromDTO = (deviceTypeDTO: DeviceTypeDTO): DeviceType => {
  switch (deviceTypeDTO) {
    case DeviceTypeDTO.Car:
      return DeviceType.Car;
    case DeviceTypeDTO.Ambulance:
      return DeviceType.Ambulance;
    case DeviceTypeDTO.Truck:
      return DeviceType.Truck;
    case DeviceTypeDTO.Indoor:
      return DeviceType.Indoor;
    default:
      throw new Error('Unknown device type received from backend service');
  }
};

export const mapDeviceFromDTO = (deviceDTO: DeviceDTO): Device => ({
  imei: deviceDTO.imei,
  name: deviceDTO.name,
  location: [deviceDTO.longitude, deviceDTO.latitude],
  accelerometer: deviceDTO.accelerometer,
  temperature: deviceDTO.temperature,
  battery: deviceDTO.battery,
  historical: deviceDTO.historical,
  type: mapDeviceTypeFromDTO(deviceDTO.type),
});

export const mapDeviceOptionFromDTO = (deviceDTO: DeviceDTO): DeviceOption => ({
  label: deviceDTO.name,
  value: deviceDTO.imei
});

