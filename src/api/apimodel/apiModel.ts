// Common API model entities shared among different endpoints

export enum DeviceTypeDTO {
  Car = "Car",
  Ambulance = "Ambulance",
  Truck = "Truck",
  Indoor = "Indoor"
}

export type LatLng = [number, number];

export interface DeviceDTO {
  imei: string;
  name: string;
  latitude: number;
  longitude: number;
  accelerometer: number;
  temperature: number;
  battery: number;
  historical: LatLng[];
  type: DeviceTypeDTO;
}