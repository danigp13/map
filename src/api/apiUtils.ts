import { AxiosError } from "axios";

export const serverURL = "http://localhost:3001";


export const errorCodes = {
  ERROR_FETCH_DEVICES: 'Could not fetch devices',
  ERROR_SEARCH_DEVICES: 'Could not search devices',
};

export const buildErrorMessage = (message: string) => (error: AxiosError): string => {
  if (error && error.response) {
    let errorMessage: string;
    switch (error.response.status) {
      case 400:
        errorMessage = 'Wrong request. Server was not able to process the request';
        break;
      case 401:
        errorMessage = 'User authentication failed. User does not have the necessary credentials to access this resource';
        break;
      case 403:
        errorMessage = 'Access failure. User does not have the necessary permissions to access this resource';
        break;
      case 404:
        errorMessage = 'Data requested could not be found in server/database';
        break;
      default:
        errorMessage = `ERROR. Status ${error.response.status}: ${error.response.statusText}. ${message}`;
        break;
    }
    return errorMessage;
  } else if (error && error.request) {
    return `ERROR. ${message}. Request was sent but server provided no response.
    A possible reason could be the request timeout configuration.
    Please, check it with your local administrator.`;
  } else {
    return `ERROR. ${message}. Unexpected error, request was not sent`;
  }
};