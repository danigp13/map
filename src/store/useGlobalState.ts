import { useMemo, useState } from "react";
import { fetchDevices } from "../api";
import { Device } from "../model";

export const useGlobalState = () => {
  const [selectedDevice, setSelectedDevice] = useState<string>("");
  const [selectedType, setSelectedType] = useState<string[]>([]);
  const [deviceData, setDeviceData] = useState<Device[]>([]);

  const searchDeviceByType = useMemo(
    () => () => {
      fetchDevices(selectedType)
      .then(result => {
        setDeviceData(result);
      });
    },
    [selectedType, setDeviceData]
  );

  return {
    selectedDevice,
    setSelectedDevice,
    selectedType,
    setSelectedType,
    searchDeviceByType,
    deviceData,
  };
};