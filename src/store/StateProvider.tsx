import React, { ReactNode } from 'react';
import { Context } from './Context';
import { useGlobalState } from './useGlobalState';

export interface StateProviderProps {
  children: ReactNode;
}

export const StateProvider: React.FC<StateProviderProps> = ({ children } : StateProviderProps) => {
  const store = useGlobalState();

  return <Context.Provider value={store}>{children}</Context.Provider>;
};