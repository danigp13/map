import React from "react";
import { Device } from "../model";

interface DefaultContext {
  selectedDevice: string;
  setSelectedDevice: (id: string) => void;
  selectedType: string[];
  setSelectedType: (types: string[]) => void;
  searchDeviceByType: () => void;
  deviceData: Device[];
}

export const Context = React.createContext<DefaultContext>({
  selectedDevice: "",
  setSelectedDevice: () => {},
  selectedType: [],
  setSelectedType: () => {},
  searchDeviceByType: () => {},
  deviceData: [],
});