import { LatLngExpression } from "leaflet";

export enum DeviceType {
  Car = "Car",
  Ambulance = "Ambulance",
  Truck = "Truck",
  Indoor = "Indoor"
}

export interface Device {
  imei: string,
  name: string,
  location: LatLngExpression;
  accelerometer: number;
  temperature: number;
  battery: number;
  historical: LatLngExpression[];
  type: DeviceType,
}

export interface DeviceOption {
  label: string;
  value: string;
}


export interface LngLatBounds {
  ne: LngLat;
  sw: LngLat;
  nw?: LngLat;
  se?: LngLat;
}

export interface LngLat {
  lng: number;
  lat: number;
}