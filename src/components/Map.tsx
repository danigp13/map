/* eslint-disable react-hooks/exhaustive-deps */
import { DivIcon, LatLngExpression } from 'leaflet';
import { ReactNode, useEffect, useRef } from 'react';
import { MapContainer, Marker, Popup, TileLayer, Polyline, GeoJSON, useMapEvents } from 'react-leaflet';
import { Device, DeviceType } from '../model';
import geoJSONData from '../leaflet/indoorGeoJson.json';
import { useStore } from '../store';
import { getBoundsFromPoints, mapBounds, mapLocation, AmbulanceIcon, CarIcon, DefaultIcon, TruckIcon } from '../utils';

export interface MarkerMap {
  [id: string]: any;
}

export const Map = () => {
  const {selectedDevice, setSelectedDevice, deviceData} = useStore();

  const mapRef = useRef<any>(null);

  const markerRefs = useRef<MarkerMap>({});

  useEffect(() => {
    mapRef?.current?.fitBounds(mapBounds(getBoundsFromPoints(deviceData?.map(d => mapLocation(d.location as number[])))));
  }, [deviceData]);

  useEffect(() => {
    openPopup();
  }, [selectedDevice]);

  const openPopup = () => {
    if (selectedDevice) {
      const device: Device | null = deviceData?.find(d => d.name === selectedDevice) ?? null;
      mapRef?.current?.setView(device?.location as LatLngExpression, mapRef?.current?.getZoom());
      const markerToOpen = device?.imei !== undefined ? markerRefs?.current[device?.imei] : null;
      markerToOpen?.openPopup();
    }
  }

  const Bounds = () => {
    useMapEvents({
      click(e: any) {
        setSelectedDevice('');
      },
    });
    return null;
  }

  const renderMarkerIcon = (deviceType: DeviceType): DivIcon => {
    switch (deviceType) {
      case DeviceType.Car:
        return CarIcon;
      case DeviceType.Ambulance:
          return AmbulanceIcon;
      case DeviceType.Truck:
        return TruckIcon;
      default:
        return DefaultIcon;
    }
  }
  const renderMarkers = (data: Device[]): ReactNode => (
    data.map((d, i) => (
      <Marker
        ref={(m) => {
          markerRefs.current[d.imei] = m;
        }}
        key={d.imei} position={d.location} eventHandlers={{
        click: () => {
          setSelectedDevice(d.name);
        },
      }} icon={renderMarkerIcon(d.type)}>
        <Popup>
          Name: {d.name}<b/><br />Accelerometer: {d.accelerometer}<br />temperature: {d.temperature}<br />Battery: {d.battery}<br /> 
        </Popup>
      </Marker>
    ))
  );

  const geojsonData: GeoJSON.FeatureCollection = geoJSONData as GeoJSON.FeatureCollection;

  return (
    <MapContainer ref={mapRef} zoom={13} scrollWheelZoom={false}>
      <TileLayer
        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        url='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
      />
      {renderMarkers(deviceData)}
      <Polyline color='black' positions={deviceData?.find(d => d.name === selectedDevice)?.historical ?? []} />
      <GeoJSON key='my-geojson' data={geojsonData} />
      <Bounds />
    </MapContainer>
  );
}