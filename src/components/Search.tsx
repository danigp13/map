import React, { useEffect, useRef } from 'react';

import AsyncSelect from 'react-select/async';
import { searchDevices } from '../api';
import { DeviceOption } from '../model';
import { useStore } from '../store';

export const Search: React.FC = () => {

  const {setSelectedDevice, selectedType, deviceData, selectedDevice} = useStore();

  const selectInputRef = useRef<any>(null);

  useEffect(() => {
    selectInputRef?.current?.select?.clearValue();
  }, [selectedType]);

  const handleChange = (e: any) => {
    setSelectedDevice(e.label);
  }

  const deviceOptions: DeviceOption[] = deviceData.map(d => ({label: d.name, value: d.imei}));
  const value: DeviceOption | null = deviceOptions.find(d => d.label === selectedDevice) ?? null;

  return (
    <AsyncSelect ref={selectInputRef} value={value} cacheOptions defaultOptions={deviceOptions} loadOptions={searchDevices(selectedType)} placeholder={'Search a device'} /*styles={colourStyles}*/ onChange={handleChange} />
  )
};