import * as React from 'react'
import { CommonLayout } from './CommonLayout';

export const System: React.FC = () => {
  return (
    <CommonLayout>
      <h1>System</h1>
    </CommonLayout>
  );
};