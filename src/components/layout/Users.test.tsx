import { render, screen } from '@testing-library/react';
import { Users } from './Users';
import { CommonLayoutProps } from './CommonLayout';

jest.mock('./CommonLayout', () => ({
  CommonLayout: (props: CommonLayoutProps) => <div>{props?.children}</div>,
}));


describe('Users', () => {
  it('should render as expected', () => {
    render(<Users />);

    const title = screen.getByText('Users');

    expect(title).toBeInTheDocument();
  });
});