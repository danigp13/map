import { render, screen } from '@testing-library/react';
import { Alarms } from './Alarms';
import { CommonLayoutProps } from './CommonLayout';

jest.mock('./CommonLayout', () => ({
  CommonLayout: (props: CommonLayoutProps) => <div>{props?.children}</div>,
}));


describe('Alarms', () => {
  it('should render as expected', () => {
    render(<Alarms />);

    const title = screen.getByText('Alarms');

    expect(title).toBeInTheDocument();
  });
});