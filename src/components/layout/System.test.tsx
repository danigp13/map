import { render, screen } from '@testing-library/react';
import { System } from './System';
import { CommonLayoutProps } from './CommonLayout';

jest.mock('./CommonLayout', () => ({
  CommonLayout: (props: CommonLayoutProps) => <div>{props?.children}</div>,
}));


describe('System', () => {
  it('should render as expected', () => {
    render(<System />);

    const title = screen.getByText('System');

    expect(title).toBeInTheDocument();
  });
});