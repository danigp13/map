import * as React from 'react'
import { Link, useLocation } from 'react-router-dom';

export interface CommonLayoutProps {
  children: React.ReactNode;
}

export const CommonLayout: React.FC<CommonLayoutProps> = ({ children } : CommonLayoutProps) => {
  const { pathname } = useLocation();
  return (
    <div className='App'>
      <div className='header'>
        
        <div className='header-title bold'>ACME Ltd.</div>
        <div className='header-links'>
          <ul>
            <li><Link className={`${pathname === '/' ? 'bold' : ''}`} to='/'>Home</Link></li>
            <li><Link className={`${pathname === '/alarms' ? 'bold' : ''}`} to='/alarms'>Alarms</Link></li>
            <li><Link className={`${pathname === '/users' ? 'bold' : ''}`} to='/users'>Users</Link></li>
            <li><Link className={`${pathname === '/system' ? 'bold' : ''}`} to='/system'>System</Link></li>
          </ul>
        </div>
      </div>
      {children}
    </div>
  );
};