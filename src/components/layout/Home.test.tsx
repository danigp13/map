import { render, screen } from '@testing-library/react';
import { Home, TEST_IDS } from './Home';
import { CommonLayoutProps } from './CommonLayout';

jest.mock('./CommonLayout', () => ({
  CommonLayout: (props: CommonLayoutProps) => <div>{props?.children}</div>,
}));

jest.mock('../Map', () => ({
  Map: () => <div>map</div>,
}));

describe('Home', () => {
  it('should render as expected', () => {
    render(<Home />);

    const title = screen.getByText('Asset Tracking');

    expect(title).toBeInTheDocument();
    expect(screen.getByTestId(TEST_IDS.containerHeaderTitle)).toBeTruthy();
    expect(screen.getByTestId(TEST_IDS.containerHeaderContent)).toBeTruthy();
    expect(screen.getByTestId(TEST_IDS.containerHeaderTitle)).toBeTruthy();
    expect(screen.getByTestId(TEST_IDS.containerMain)).toBeTruthy();
  });
});