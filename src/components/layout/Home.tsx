import * as React from 'react';
import { Map } from '../Map';
import { CommonLayout } from './CommonLayout';
import { Search } from '../Search';
import { Tree } from '../Tree';
import { useStore } from '../../store';

export const TEST_IDS = {
  containerHeaderTitle: 'home-container-header-title',
  containerHeaderContent: 'home-container-header-content',
  containerAside: 'home-container-aside',
  containerMain: 'home-container-main',
};

export const Home: React.FC = () => {


  const {searchDeviceByType} = useStore();


  React.useEffect(() => {
    searchDeviceByType();
  }, [searchDeviceByType]);


  return (
    <CommonLayout>
      <div className='container-header'>
        <div className='container-header-title bold' data-testid={TEST_IDS.containerHeaderTitle}>
          Asset Tracking
        </div>
        <div className='container-header-content' data-testid={TEST_IDS.containerHeaderContent}>
          <div className='container-header-content-search'>
            <Search />
          </div>
        </div>
      </div>
      <div className='container'>
        <aside className='opened drawer' data-testid={TEST_IDS.containerAside}>
          <Tree />
        </aside>
        <main className='main' data-testid={TEST_IDS.containerMain}>
          <Map />
        </main>
      </div>
    </CommonLayout>
  );
};
