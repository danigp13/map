import * as React from 'react';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import FolderOpen from '@mui/icons-material/FolderOpen';
import FileOpen from '@mui/icons-material/FileOpen';
import TreeItem, {  } from '@mui/lab/TreeItem';
import TreeView from '@mui/lab/TreeView';
import Button from '@mui/material/Button';
import { useStore } from '../store';
import { dataTree, getNodes, isLeaf, isParent } from '../utils';

export interface RenderTree {
  id: string;
  name: string;
  children?: readonly RenderTree[];
}


export const Tree: React.FC = () => {
  const {searchDeviceByType, setSelectedType, selectedType, setSelectedDevice} = useStore();
  const [expanded, setExpanded] = React.useState<string[]>([]);

  React.useEffect(() => {
    searchDeviceByType();
  }, [setSelectedType, searchDeviceByType]);

  const handleToggle = (event: React.SyntheticEvent, nodeIds: string[]) => {
    setExpanded(nodeIds);
  };

  const handleSelect = (event: React.SyntheticEvent, nodeIds: string[]) => {
    // Compare array by content because are objects and === doesnt work because compare references
    const isSameArrayContent: boolean = JSON.stringify(selectedType) === JSON.stringify(nodeIds);
    // If it was selected, deselect
    setSelectedType(isSameArrayContent ? [] : getNodes([dataTree], isLeaf).map(e => e.id).filter(item => nodeIds.includes(item)));
    setSelectedDevice('');
  };

  const handleExpandClick = () => {
    setExpanded((oldExpanded) =>
      oldExpanded.length === 0 ? getNodes([dataTree], isParent).map(e => e.id) : [],
    );
  };

  const renderTree = (nodes: RenderTree) => (
    <TreeItem key={nodes.id} nodeId={nodes.id} label={nodes.name}>
      {Array.isArray(nodes.children)
        ? nodes.children.map((node) => renderTree(node))
        : null}
    </TreeItem>
  );

  return (
    <React.Fragment>
      <Button onClick={handleExpandClick}>
        {expanded.length === 0 ? 'Expand all' : 'Collapse all'}
      </Button>
      <TreeView
        aria-label='rich object'
        defaultCollapseIcon={<FolderOpen />}
        expanded={expanded}
        selected={selectedType}
        onNodeToggle={handleToggle}
        onNodeSelect={handleSelect}
        defaultExpandIcon={<ChevronRightIcon />}
        defaultEndIcon={<FileOpen />}
        multiSelect
        sx={{ height: 600, flexGrow: 1, textAlign: 'left'}}
      >
        {renderTree(dataTree)}
      </TreeView>
    </React.Fragment>
  );
}