import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { Alarms, Users, System, Home } from './components';
import './css/App.css';
import { StateProvider } from './store';

export const AppRouter: React.FunctionComponent = () => {
  return (
    <StateProvider>
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='/alarms' element={<Alarms />} />
          <Route path='/users' element={<Users />} />
          <Route path='/system' element={<System />} />
        </Routes>
      </BrowserRouter>
    </StateProvider>
  );
};


