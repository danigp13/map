# Getting Started

  

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).


To start the application locally, you only have to do `npm install` to install all the dependencies and once the dependencies are installed, `npm start` and you will start simultaneously the server and the client.

  

## Available Scripts

  

In the project directory, you can run:

  

### `npm start`

  

Runs the app in the development mode.\ You will start concurrently the client and server (json-server is being used to mock the api).

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

  

The page will reload if you make edits.\

You will also see any lint errors in the console.

  

### `npm test`

  

Launches the test runner in the interactive watch mode.\

See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

  

### `npm run build`

  

Builds the app for production to the `build` folder.\

It correctly bundles React in production mode and optimizes the build for the best performance.

  

The build is minified and the filenames include the hashes.\

Your app is ready to be deployed!

  

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

  

### `npm run eject`

  

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

  

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

  

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

  

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.



# User Manual


When you load the application first time, by default a list of devices will appear. It has been created mocked data with several devices including 3 types:

- Car
- Ambulance
- Truck

You could add any device in default server/db.json inside 'devices' json object which is loaded at the beginning with next format:

```
"devices" : [
  ...
    {
      "imei":"imei",
      "name":"name",
      "latitude": -0.13732910156250003,
      "longitude": 51.49203339050825,
      "accelerometer": 1,
      "temperature":38.2,
      "battery": 80,
      "historical":[
        [51.49203339050825, -0.13732910156250003], [51.48968011390214, -0.13269424438476565], [51.4854011179502, -0.12239456176757814]
      ],
      "type": "Car" | "Ambulance" | "Truck"
    }
  ...
  ]
```


The application is composed by a Navigation pane with different options (Home (by default), Alarms, User, System).

Only has been develop Home (you can navigate between all the links but only have some default content).

Regarding Home option, it contains:

- **Tree:** showing the structure of the files loaded (due to the data to fill this component is recursive, it haven´t added to db.json because is not a json itself. Anyway, you can change dinamically to load other options in the tree). You can select a leaf option in the tree (The type of the data) and the devices will be filtered and showing in map. If you click again, it will deselect.

- **Search:** shows a search bar with available devices filter by selected type in tree, and if you select one, it will be focus in the map and selected. If the element is selected, it will be focus in the map and a popup will be shown with information of the device just as the breadcum of the device.

- **Map:** shows the map of the application representing the devices in the map. Every device has an specific icon assigned and can be clicked to show information and breadcum.
- 

![Overview](/assets/images/map.png)

